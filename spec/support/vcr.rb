require 'vcr'
require 'webmock'

VCR.configure do |config|
  WebMock.enable!
  config.cassette_library_dir = "spec/vcr_cassettes"
  config.hook_into :webmock
end
